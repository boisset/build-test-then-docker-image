#!/bin/sh

export TAG=`git describe`

./mvnw package && cp ./target/demo*.jar demo-$TAG.jar && echo OK
